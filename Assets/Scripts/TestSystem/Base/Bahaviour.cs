﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YF {
    [Serializable]
    public abstract class Bahaviour
    {
        public delegate void OnAwake();
        public delegate void OnStart();
        public delegate void OnEnable();
        public delegate void OnDisable();
        public delegate void OnUpdate();
        public delegate void OnFixedUpdate();
        public delegate void OnDestroy();

        public OnAwake onAwake = delegate { };

        public virtual void GetName()
        {
            Debug.Log(this.GetType());
        }

        public OnStart onStart = delegate { };
        public OnEnable onEnable= delegate { };
        public OnDisable onDisable = delegate { };
        public OnUpdate onUpdate= delegate { };
        public OnFixedUpdate onFixedUpdate= delegate { };
        public OnDestroy onDestroy = delegate { };

        protected SystemComponent _systemComponent;

        public virtual void Intilize() {
            onAwake += _OnAwake;
            onStart += _OnStart;
            onUpdate += _OnUpdate;
            onFixedUpdate += _OnFixedUpdate;
            onEnable += _OnEnable;
            onDisable += _OnDisable;
            onDestroy += _OnDestroy;
        }

        public virtual void _OnAwake() {
          
        }

        public virtual void _OnStart() {
        }

        public virtual void _OnEnable() {

        }

        public virtual void _OnDisable() { 
        
        }
        public virtual void _OnUpdate() { 
        
        }

        public virtual void _OnFixedUpdate() { 
        
        }

        public virtual void _OnDestroy() {
            onAwake += _OnAwake;
            onStart += _OnStart;
            onUpdate += _OnUpdate;
            onFixedUpdate += _OnFixedUpdate;
            onEnable += _OnEnable;
            onDisable += _OnDisable;
            onDestroy += _OnDestroy;
        }

        public virtual void Bind(SystemComponent systemComponent) {
            this._systemComponent = systemComponent;
        }

    }
}
