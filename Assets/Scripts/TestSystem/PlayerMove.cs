﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YF
{
    [Serializable]
    public class PlayerMove : Bahaviour
    {
        [SerializeField]
        float speed;
        Player player;

        public override void Intilize()
        {
            onStart += _OnStart;
            onUpdate += _OnUpdate;
        }

        public override void _OnStart()
        {
            player = (Player)_systemComponent;
            Debug.Log("start " + player.name);
        }

        public override void _OnUpdate()
        {
            player.transform.position += (Vector3)player.playerInput.velocity*Time.deltaTime*speed;
        }

    }
}