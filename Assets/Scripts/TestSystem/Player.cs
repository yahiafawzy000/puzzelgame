﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YF {
    public class Player : SystemComponent
    {
        [Header("Player Input")]
        public PlayerInput playerInput;

        [Header("player Damage")]
        public PlayerDamage playerDamage;

        [Header("player Movment")]
        public PlayerMove playerMove;
    } 
}
