﻿using System;
using UnityEngine;

namespace YF
{
    [Serializable]
    public class PlayerInput : Bahaviour
    {
        [HideInInspector]
        public Vector2 velocity;

        public override void Intilize()
        {
            onUpdate += _OnUpdate;
        }

        public override void _OnStart()
        {
            Debug.Log("PlayerInput");
        }

        public override void _OnUpdate()
        {            
            velocity = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        }
    }
}
