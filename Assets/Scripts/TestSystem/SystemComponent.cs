﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using YF;

public class SystemComponent : MonoBehaviour
{
    private List<Bahaviour> bahaviours = new List<Bahaviour>();
    public virtual void Awake()
    {
        Type type = this.GetType();
        foreach (var field in type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
        {
            Bahaviour bahaviour = (Bahaviour)field.GetValue(this);
            if (bahaviour!=null)
            {
                bahaviour.Bind(this);
                bahaviours.Add(bahaviour);
                bahaviour.Intilize();
                bahaviour.onAwake?.Invoke();
            }
        }      
    }


    public virtual void Start() {
        foreach (Bahaviour bahaviour in bahaviours) {
            bahaviour.onStart?.Invoke();
        }
    }

    public virtual void Update()
    {
        foreach (Bahaviour bahaviour in bahaviours)
        {
            bahaviour.onUpdate?.Invoke();
        }
    }

    public virtual void FixedUpdate()
    {
        foreach (Bahaviour bahaviour in bahaviours)
        {
            bahaviour.onUpdate?.Invoke();
        }
    }



}

