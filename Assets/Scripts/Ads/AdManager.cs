﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Events;

public class AdManager : MonoBehaviour, IUnityAdsListener
{
    public string gameId = "3878755";
    public string bannerID = "bannerAd";
    private static readonly string videoID = "PuzzelGameVedioId01097145492";
    private static readonly string rewardedID = "rewardedVideo";

    [SerializeField]
    bool testMode;

    //public delegate void AdSuccess();
    //public delegate void AdSkipped();
    //public delegate void AdFailed();
    //private AdSuccess adSuccess= delegate { };
    //private AdSkipped adSkipped = delegate { };
    //private AdFailed adFailed = delegate { };
    public static AdManager instance;

    public delegate void RewardAdd();
    public RewardAdd rewardAdd = delegate { };

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            Advertisement.AddListener(this);
            Advertisement.Initialize(gameId, testMode);
            ShowBanner();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void ShowBanner()
    {
        StartCoroutine(ShowBannerWhenReady());
    }

    public void HideBanner()
    {
        Advertisement.Banner.Hide();
    }

    public void ShowRewardedAd(RewardAdd reward = null/*AdSuccess success=null, AdSkipped skipped=null, AdFailed failed=null*/)
    {
        //if(success!=null)
        //instance.adSuccess += success;
        //if(skipped!=null)
        //instance.adSkipped += skipped;
        //if(failed!=null)
        //instance.adFailed += failed;
        rewardAdd = null;
        rewardAdd += reward;
        if (Advertisement.IsReady(rewardedID))
            Advertisement.Show(rewardedID);
    }

    private IEnumerator ShowBannerWhenReady()
    {
        //yield return Advertisement.isInitialized;
        while (!Advertisement.IsReady(bannerID))
            yield return new WaitForSeconds(0.5f);

        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
        Advertisement.Banner.Show(bannerID);
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        //Debug.Log("--) " + placementId);
        //rewardAdd?.Invoke();
        /*
        if (placementId == rewardedID)
        {
            switch (showResult)
            {
                case ShowResult.Finished:
                    adSuccess();
                    break;
                case ShowResult.Skipped:
                    adSkipped();
                    break;
                case ShowResult.Failed:
                    adFailed();
                    break;
            }
        } */
    }

    public void OnUnityAdsDidError(string message) { }
    public void OnUnityAdsDidStart(string placementId) {
        rewardAdd?.Invoke();
    }
    public void OnUnityAdsReady(string placementId) { }
}