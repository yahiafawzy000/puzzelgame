﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;

public class BannerAdScript : MonoBehaviour,IUnityAdsListener
{

    public string gameId = "3878755";
    public string placementId = "PuzzelGameBannerId01097145492";
    public bool testMode = true;

    void Start()
    {
        DontDestroyOnLoad(this);
        Advertisement.Initialize(gameId, testMode);       
        ShowBanner();
    }

    public void ShowBanner()
    {
        StartCoroutine(ShowBannerWhenReady());
    }

    private IEnumerator ShowBannerWhenReady()
    {
        while (!Advertisement.isInitialized)
        {
            yield return Advertisement.isInitialized;
        }
        Debug.Log("test ---------------->" + Advertisement.GetPlacementState());
        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
        Advertisement.Banner.Show(placementId);
    }

    IEnumerator ShowBannerWhenInitialized()
    {
        Debug.Log("1--->" + Advertisement.GetPlacementState());
        while (!Advertisement.IsReady(placementId))
        {
            Debug.Log("2--->" + Advertisement.GetPlacementState());
            yield return new WaitForSeconds(0.5f);
        }
        Debug.Log("3--->" + Advertisement.GetPlacementState());
        //LoadBanner();
        //Advertisement.Banner.Load(placementId);
        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
        Advertisement.Banner.Show(placementId);
    }

    public void LoadBanner()
    {
        //if (!Advertisement.Banner.isLoaded)
        //{
        //    BannerLoadOptions loadOptions = new BannerLoadOptions
        //    {
        //        loadCallback = OnBannerLoaded,
        //        errorCallback = OnBannerError
        //    };
           
        //}
    }

    public void OnUnityAdsReady(string placementId)
    {
        Debug.Log("load add ");
        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
        Advertisement.Banner.Show(placementId);
    }

    public void OnUnityAdsDidError(string message)
    {
        Debug.Log("OnUnityAdsDidError ");

        //throw new System.NotImplementedException();
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        Debug.Log("OnUnityAdsDidStart ");
        //throw new System.NotImplementedException();
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        Debug.Log("OnUnityAdsDidFinish ");

        // throw new System.NotImplementedException();
    }
}