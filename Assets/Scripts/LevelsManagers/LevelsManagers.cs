﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelsManagers : MonoBehaviour
{
    [SerializeField]
    GameObject []levelSPrefabs;
    public static LevelsManagers instance;
    private int currentLevelIndx = -1;

    public GameObject LevelParent, Level;
    internal int CurrentLevel { get => currentLevelIndx; set => currentLevelIndx = value; }
    GameObject currentLevel,prevLevel;

    private void OnEnable()
    {
        
    }
    private void Awake()
    {
        instance = this;        
    }

    public void OpenLevel(int levelIndex) {
        if (prevLevel)
        {
            Destroy(prevLevel);
        }
        currentLevel = Instantiate(levelSPrefabs[levelIndex], LevelParent.transform);
        prevLevel = currentLevel;
        currentLevelIndx=levelIndex;
    }

    public void LoadLevel(int levelIndex) {       
        currentLevel = Instantiate(levelSPrefabs[levelIndex], LevelParent.transform);
        currentLevel.SetActive(false);
        currentLevelIndx = levelIndex;
    }

    public void ActiveLevel() {
        Level.gameObject.SetActive(true);
        //Level.transform.SetAsLastSibling();
        if (prevLevel)
        {
            Destroy(prevLevel);
        }
        prevLevel = currentLevel;
        currentLevel.SetActive(true);
    }

    
}
