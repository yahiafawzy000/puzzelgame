﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace yf
{
    public class StringCodeManager : MonoBehaviour, ICode
    {
        [SerializeField]
        RawImage targetImage;
        [SerializeField]
        Texture target;

        [SerializeField]
        TMPro.TMP_Text[] txts;

        [SerializeField]
        string[] allStrings;
      
        [SerializeField]
        string code, solve;
        [SerializeField]
        int[] arrOfInex;
        private void Start()
        {
            arrOfInex = new int[txts.Length];

            for (int i = 0; i < txts.Length; i++)
            {
                arrOfInex[i] = -1;

                arrOfInex[i] = -1;
                txts[i].text = "-";
                //  txts[i].color = new Vector4(1, 1, 1, 0);
            }
        }

        public void SetCode(int index)
        {
            //codeImages[index].color = new Vector4(1, 1, 1, 1);
            arrOfInex[index] = (arrOfInex[index] < allStrings.Length - 1) ? arrOfInex[index] + 1 : 0;
            txts[index].text = allStrings[arrOfInex[index]];
        }

        public void MatchCode()
        {
            solve = "";
            for (int i = 0; i < arrOfInex.Length; i++)
            {
                solve += arrOfInex[i];
            }
            if (code.Equals(solve))
            {
                Debug.Log("Solved");
                targetImage.texture = target;
                foreach (Action action in GetComponents<Action>())
                {
                    action.Execute();
                }
            }
            else
            {
                Debug.Log("code error");
            }
        }
    }
}