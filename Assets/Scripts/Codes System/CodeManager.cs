﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace yf {
    public class CodeManager : MonoBehaviour,ICode
    {
        [SerializeField]
        RawImage targetImage;
        [SerializeField]
        RawImage[] codeSlots;
        [SerializeField]
        string code;

        [SerializeField]
        Texture target;

        private char[] codeArray = new char[3];
        public Color[] colors;
        private int[] colorIndexs = { 0, 0, 0 };

        private void Start()
        {
            for (int i = 0; i < codeSlots.Length; i++)
            {
                codeSlots[i].color = colors[0];
            }
        }
        public void SetCode(int index)
        {
            colorIndexs[index] = (colorIndexs[index] == 3) ? 0 : colorIndexs[index] + 1;
            codeSlots[index].color = colors[colorIndexs[index]];
        }

        public void MatchCode() {
            if (code.Equals(colorIndexs[0] + "" + colorIndexs[1] + "" + colorIndexs[2]))
            {
                targetImage.texture = target;
                foreach (Action action in GetComponents<Action>())
                {
                    action.Execute();
                }
            }
            else {
                Debug.Log("code error");
            }
        }

    } 

}
