﻿using UnityEngine;
using UnityEngine.UI;

namespace yf
{
    public class TextCodeManager : MonoBehaviour,ICode
    {
        [SerializeField]
        RawImage targetImage;     

        [SerializeField]
        Texture target;

        [SerializeField]
        TMPro.TMP_Text []txts;

        public string Code;
        public string AlphaBit = "ABC";
        public string Solve;
        [SerializeField]
        int [] arrOfInex;
        private void Start()
        {
            arrOfInex = new int[txts.Length];
            for (int i = 0; i < arrOfInex.Length; i++)
            {
                arrOfInex[i] = -1;
            }
        }

        public void SetCode(int index)
        {
            arrOfInex[index] = (arrOfInex[index] < AlphaBit.Length-1) ? arrOfInex[index]+1 : 0;
            txts[index].text = AlphaBit[arrOfInex[index]].ToString();
        }
        public void MatchCode()
        {
            Solve = "";
            for (int i = 0; i < txts.Length; i++) {
                Solve += AlphaBit[arrOfInex[i]].ToString();
            }
            if (Code.Equals(Solve))
            {
                Debug.Log("Solved");
                targetImage.texture = target;
                foreach (Action action in GetComponents<Action>())
                {
                    action.Execute();
                }
            }
            else {
                Debug.Log("code error");
            }
        }
    }
}