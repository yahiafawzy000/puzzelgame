﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "GameSetting")]
[Serializable]
public class GameSettingScriptaleObject : ScriptableObject
{
    public int framePerSeconds;   
}