﻿using UnityEngine;

public class PrefrencesManager
{   
    public static void ClearCash() {
        PlayerPrefs.SetInt("openLevels", 1);
        PlayerPrefs.SetInt("closedLevels", 11);
        PlayerPrefs.SetInt("unAvalableLevels", 20);
    }

    public static int OpenLevels { 
        get => PlayerPrefs.GetInt("openLevels", 1);
        set => PlayerPrefs.SetInt("openLevels", value);
    }
    public static int ClosedLevels {
        get => PlayerPrefs.GetInt("closedLevels",11);
        set => PlayerPrefs.SetInt("closedLevels", value);
    }
    public static int UnAvalableLevels {
        get => PlayerPrefs.GetInt("unAvalableLevels", 20);
        set => PlayerPrefs.SetInt("unAvalableLevels", value);
    }

    public static int Music { 
        get => PlayerPrefs.GetInt("Music", 1);
        set => PlayerPrefs.SetInt("Music", value);
    }

    public static int LevelToOpen
    {
        get => PlayerPrefs.GetInt("Level", 0);
        set => PlayerPrefs.SetInt("Level", value);
    }

    public static int Sound
    {
        get => PlayerPrefs.GetInt("Sound", 1);
        set => PlayerPrefs.SetInt("Sound", value);
    }
}
