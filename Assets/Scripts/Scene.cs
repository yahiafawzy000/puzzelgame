﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scene : MonoBehaviour
{
    public Texture target;

    [SerializeField]
    GameObject[] toEnable,toDisable;

    private void OnEnable()
    {
        GetComponent<RawImage>().texture = target;
        foreach (GameObject gameObj in toEnable) {
            if (gameObj)
                gameObj.SetActive(true);
        }
        foreach (GameObject gameObj in toDisable)
        {
            if (gameObj)
            {
                gameObj.SetActive(false);
            }
        }
    }

    public void CheckImageToSetDefault(Texture texture) {
        if (GetComponent<RawImage>().texture == texture)
        {
            GetComponent<RawImage>().texture = target;
        }
        else {
            GetComponent<RawImage>().texture = texture;
        }
    }

}
