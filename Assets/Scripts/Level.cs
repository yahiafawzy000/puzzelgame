﻿using UnityEngine;

public class Level : MonoBehaviour
{   
    public static Level level;
    private void Awake() {
        level = this;        
    }
    public void ActiveLevel(bool state)
    {
        gameObject.SetActive(state);
    }
}
