﻿using UnityEngine;
using System.Collections;

public class MasksManager : MonoBehaviour
{
    [SerializeField]
    GameObject []Masks;

    public static MasksManager instance;
    private void Awake()
    {
        instance = this;
    }
    internal void HideMask(int id) {
        if (id >= 0 && id < Masks.Length) {
            if (Masks[id] != null) {
               Destroy(Masks[id]);
            }
        }
    }
}
