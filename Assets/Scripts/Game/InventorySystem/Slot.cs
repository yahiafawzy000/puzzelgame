﻿using UnityEngine;
using UnityEngine.UI;
using yf;

public class Slot : Action
{
    public bool selected = false;
    public int _itemId;
    [HideInInspector]
    public Texture _icon;
    protected RawImage iconImage,parentImage;
    public static Slot selectedSlot;

    [HideInInspector]public bool keepObjectForever;
    [HideInInspector] public bool keepObjectPreview;


    public virtual void SwitchSlot(bool closePreview = true) {
        selected = !selected;
        if (parentImage == null) {
            parentImage = transform.parent.GetComponent<RawImage>();
        }

        Slot previousSlot = selectedSlot;

        if (selected)
        {
            parentImage.texture = InventoryMenu.instance.activeSlotTexture;
            selectedSlot = this;
            InventoryMenu.instance.SetSelectedSlot(this);
        }
        else {
            parentImage.texture = InventoryMenu.instance.defaultSlotTexture;
            if (previousSlot == this)
            {
                selectedSlot = null;
                InventoryMenu.instance.SetSelectedSlot(null);
            }
            return;
        }

        if (previousSlot != null)
        {
            previousSlot.SwitchSlot();
            Debug.Log("T3");
        }
    }

    public virtual void SwitchSlotOn()
    {
        Slot previousSlot = selectedSlot;

        if (selected) {
            selectedSlot = this;
            return;
        }
        Debug.Log("T2");

        selected = true;
        if (parentImage == null)
        {
            parentImage = transform.parent.GetComponent<RawImage>();
        }


        if (selected)
        {
            parentImage.texture = InventoryMenu.instance.activeSlotTexture;
            selectedSlot = this;
            InventoryMenu.instance.SetSelectedSlot(this);
        }       

        if (previousSlot != null)
        {
            Debug.Log("T4 "+previousSlot.GetType());
            previousSlot.SwitchSlot();
        }
    }


    public virtual void BindSlot(PickupItem item) {      
        _itemId = item.itemId;
        _icon = item.itemIcon;
        keepObjectForever = item.KeepForEver;
        keepObjectPreview = item.keepObjectPreview;
        iconImage = GetComponent<RawImage>();
        gameObject.SetActive(true);
        if (iconImage) {
            iconImage.texture = _icon;
        }
    }

    public override void Execute()
    {
        SwitchSlot();
    }
}
