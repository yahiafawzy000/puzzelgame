﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace yf
{
    public class ItemViewMenu : MonoBehaviour
    {
        public static ItemViewMenu instance;
        public ActableSlot actableSlot;

        Slot resultSlot;

        public GameObject itemViewrParent;
        private RawImage itemViewrIcon;

        private void Awake()
        {
            instance = this;
            itemViewrIcon = itemViewrParent.transform.GetChild(1).transform.GetChild(1).GetComponent<RawImage>();
        }

        public void ClosePreView()
        {
            Debug.Log("close");
            this.actableSlot = null;
            itemViewrParent.SetActive(false);
        }

        public void OpenPreview(ActableSlot actableSlot) //4
        {
            Debug.Log("open");
            this.actableSlot = actableSlot;
            itemViewrParent.gameObject.SetActive(true);
            itemViewrIcon.texture = actableSlot._bigIcon;
        }

        public void ViewerCliked() { //5
            Slot slot = Slot.selectedSlot;
            if (!slot) return;
            if (actableSlot._actableItself) {
                if (slot == actableSlot)
                {
                    Debug.Log("145");
                    Slot.selectedSlot = null;
                }
                UpdtateCurrentSlot(actableSlot);    
            }
            else if (actableSlot._itemId + slot._itemId == 0)
            {
                Debug.Log("sucess");
                UpdtateCurrentSlot(actableSlot);
                ClearSlot(slot);
            }
        }

        private void UpdtateCurrentSlot(ActableSlot actableSlot)
        {
            
            if (actableSlot.actableData[0].audioClip) {
                SoundManager.instance.playSound(actableSlot.actableData[0].audioClip);
            }
            if (actableSlot.actableData[0]._resultItemType == ResultItemType.Actable)
            {
                AddActableSlot(actableSlot);
               
            }
            else {
                AddNonActableSlot(actableSlot);
            }
            

        }

        //convert actable slot to non actable slot (update slot)
        private void AddNonActableSlot(ActableSlot actableSlot)
        {
            Slot slot = actableSlot.gameObject.AddComponent<Slot>();
            slot._icon = actableSlot.actableData[0]._targrtIcon;
            slot._itemId = actableSlot.actableData[0]._ResultItemId;
            slot.keepObjectForever = actableSlot.actableData[0].KeepItEver;
            slot.keepObjectPreview = actableSlot.keepObjectPreview;
            slot.GetComponent<RawImage>().texture = actableSlot.actableData[0]._targrtIcon;
            itemViewrIcon.texture = actableSlot.actableData[0]._targrtBigIcon;

            if (!slot.keepObjectPreview)
                ClosePreView();
            Destroy(actableSlot);
            slot.SwitchSlotOn();
          

        }

        private void AddActableSlot(ActableSlot actableSlot)
        {
            if (actableSlot.actableData.Count > 0)
            {
                actableSlot.GetComponent<RawImage>().texture = actableSlot.actableData[0]._targrtIcon;
                itemViewrIcon.texture = actableSlot.actableData[0]._targrtBigIcon;
                actableSlot._icon = actableSlot.actableData[0]._targrtIcon;
                actableSlot._bigIcon = actableSlot.actableData[0]._targrtBigIcon;
                actableSlot.keepObjectForever = actableSlot.actableData[0].KeepItEver;
                actableSlot._itemId = actableSlot.actableData[0]._ResultItemId;
                actableSlot._actableItself = actableSlot.actableData[0]._actableItself;
                actableSlot.actableData.RemoveAt(0);
              
                
                if (!actableSlot.keepObjectPreview)
                    ClosePreView();
                actableSlot.SwitchOn();
            }
        }

        private void ClearSlot(Slot slot)
        {
            InventoryMenu.instance.clearSlot(slot);
        }
    }
}