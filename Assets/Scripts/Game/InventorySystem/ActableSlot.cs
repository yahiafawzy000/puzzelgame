﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace yf{
    public class ActableSlot : Slot
    {
        [Header("my data")]
        [HideInInspector]
        public Texture _bigIcon, _targrtIcon, _targrtBigIcon;
        [HideInInspector]
        public Conditions _resultCondition;
        public ResultItemType _resultItemType;
        public AudioClip _audioClip;
        public bool _actableItself = false;

        [Header("targets")]
        public List<ActableData> actableData = new List<ActableData>();
        /// <summary>
        /// select de select slot
        /// </summary>
        public override void SwitchSlot(bool closePreview=true)
        {
            selected = !selected;
            if (parentImage == null)
            {
                parentImage = transform.parent.GetComponent<RawImage>();
            }

            Slot previousSlot = selectedSlot;

            if (selected)
            {
                parentImage.texture = InventoryMenu.instance.activeSlotTexture;
                selectedSlot = this;
                ItemViewMenu.instance.OpenPreview(this);
                InventoryMenu.instance.SetSelectedSlot(this);
            }
            else
            {
                parentImage.texture = InventoryMenu.instance.defaultSlotTexture;
                if (previousSlot == this)
                {
                    selectedSlot = null;
                    if (closePreview)
                    {
                        ItemViewMenu.instance.ClosePreView();
                        InventoryMenu.instance.SetSelectedSlot(null);
                    }
                }
                return;
            }

            if (previousSlot != null)
            {
                previousSlot.SwitchSlot();
            }
        }

        public void SwitchOn() {
            Slot previousSlot = selectedSlot;

            if (selected) {
                selectedSlot = this;
                return;             
            }
            selected = true;
            if (parentImage == null)
            {
                parentImage = transform.parent.GetComponent<RawImage>();
            }


            if (selected)
            {
                parentImage.texture = InventoryMenu.instance.activeSlotTexture;
                selectedSlot = this;
                ItemViewMenu.instance.OpenPreview(this);
                InventoryMenu.instance.SetSelectedSlot(this);
            }           

            if (previousSlot != null)
            {
                previousSlot.SwitchSlot();
            }
        }

        /// bind inventory slot        
        public override void BindSlot(PickupItem item) //3
        {
            base.BindSlot(item);
            PickupActableItem actableItem = item as PickupActableItem;
            _bigIcon = actableItem._bigIcon;
            _targrtBigIcon = actableItem._targrtBigIcon;
            _targrtIcon = actableItem._targrtIcon;
            _resultCondition = actableItem._resultItemId;
            _resultItemType = actableItem._resultItemType;
            _audioClip = actableItem._audioClip;
            keepObjectPreview = item.keepObjectPreview;
            //_actableItself = 
        }

        public void BindSlotWithMulti(PickupItem item) //3
        {
            Debug.Log("3");
            base.BindSlot(item);
            PickupMultiActableItem actableItem = item as PickupMultiActableItem;
            _bigIcon = actableItem._bigIcon;
            _actableItself = actableItem._actableItself;
            actableData = actableItem.actableData;
        }
    }
}
