﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace yf
{
    public class MultiActableSlot : ActableSlot
    {
        //public List<ActableData> actableData=new List<ActableData>();

        public override void SwitchSlot(bool off=true)
        {
            selected = !selected;
            if (parentImage == null)
            {
                parentImage = transform.parent.GetComponent<RawImage>();
            }

            Slot previousSlot = selectedSlot;

            if (selected)
            {
                parentImage.texture = InventoryMenu.instance.activeSlotTexture;
                selectedSlot = this;
                ItemViewMenu.instance.OpenPreview(this);
                InventoryMenu.instance.SetSelectedSlot(this);
            }
            else
            {
                parentImage.texture = InventoryMenu.instance.defaultSlotTexture;
                if (previousSlot == this)
                {
                    selectedSlot = null;
                    ItemViewMenu.instance.ClosePreView();
                    InventoryMenu.instance.SetSelectedSlot(null);
                }
                return;
            }

            if (previousSlot != null)
            {
                previousSlot.SwitchSlot();
            }
        }

        public override void BindSlot(PickupItem item)
        {
            base.BindSlot(item);
            PickupMultiActableItem actableItem = item as PickupMultiActableItem;
            _bigIcon = actableItem._bigIcon;
            actableData = actableItem.actableData;                   
        }
    }
}


