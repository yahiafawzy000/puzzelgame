﻿using System;
using UnityEngine;
[Serializable]
public class ActableData 
{
    public Texture _targrtIcon;
    public Texture _targrtBigIcon;
    public Conditions _resultItemId;
    public ResultItemType _resultItemType;
    public int _ResultItemId;
    public AudioClip audioClip;
    public bool KeepItEver =false,KeepPreview;
    public bool _actableItself = false;

}
public enum ResultItemType
{
    Actable, NonActable
}