﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using yf;

public class InventoryMenu : MonoBehaviour
{
    public static InventoryMenu instance;

    public Slot selectedSlot;

    [Header("Inventory Slot")]
    [SerializeField]
    RawImage[] _slots;
    private PickupItem[] items = new PickupItem[6];
    public Texture defaultSlotTexture,activeSlotTexture;
    TMP_Text tMP_StageName;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else {
            Destroy(this);
        }
    }

    public void SetLevelName(int id) {
        if (tMP_StageName)
            tMP_StageName.text = "Stage " + id;
    }  

    private void Start()
    {
        tMP_StageName = transform.parent.GetChild(1).GetComponentInChildren<TMP_Text>();
        if(tMP_StageName!=null)
        tMP_StageName.text = "Stage "+ (LevelsManagers.instance.CurrentLevel+1);  
    }

    public void AddItemToInventorySlot(PickupItem item) //2
    {
        foreach (RawImage slot in _slots)
        {
            if (!slot.gameObject.activeSelf)
            {
                if (item is PickupActableItem)
                {
                    slot.gameObject.AddComponent<ActableSlot>().BindSlot(item);
                }else if (item is PickupMultiActableItem) {                 
                    slot.gameObject.AddComponent<ActableSlot>().BindSlotWithMulti(item);
                }else {
                    slot.gameObject.AddComponent<Slot>().BindSlot(item);
                }
                return;
            }
        }
    }
    public void RestInentory() {
        Slot.selectedSlot = null;
        foreach (RawImage slot in _slots)
        {
            if (slot.gameObject.activeSelf)
            {
                slot.gameObject.SetActive(false);
                Destroy(slot.GetComponent<Slot>());
            }
        }
    }

    public void SetSelectedSlot(Slot slot) {
        selectedSlot = slot;
    }

    public void clearSlot(Slot slot,bool switchOff=false) {
     
        if(switchOff)
        slot.SwitchSlot();      
        slot.gameObject.SetActive(false);
        Destroy(slot);
        slot = null; 
    }

    public void clearSlot(int id)
    {
        foreach (RawImage slot in _slots)
        {
            Slot slotComponent = slot.GetComponent<Slot>();
            if (slot.gameObject.activeSelf &&slotComponent._itemId== id) {
                if (Slot.selectedSlot != null)
                {
                    if (Slot.selectedSlot._itemId == slotComponent._itemId)
                    {
                        Slot.selectedSlot.SwitchSlot(false);
                        Slot.selectedSlot = null;
                    }
                }
                slotComponent.gameObject.SetActive(false);     
                Destroy(slotComponent);                
                break;
            }           
        }
       
    }
}

