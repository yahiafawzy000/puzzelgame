﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;
    AudioSource soundAudioSource;
    [SerializeField]
    AudioClip ButtonPressed;

    private void Awake()
    {
        Debug.Log("sss");
        instance = this;
        soundAudioSource = GetComponent<AudioSource>();
    }

    public void playSound(AudioClip audioClip)
    {
        Debug.Log(audioClip.name);
        if (PrefrencesManager.Sound == 1)
        {
            if(!soundAudioSource.isPlaying)
            soundAudioSource.PlayOneShot(audioClip);
        }
    }
      

    public void PlayButtonPressedSound()
    {
        if (PrefrencesManager.Sound == 1)       
           soundAudioSource.PlayOneShot(ButtonPressed);
    }
}
