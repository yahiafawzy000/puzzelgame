﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public static MusicManager instance;
    AudioSource musicAudioSource;

    private void Awake()
    {
        instance = this;
        musicAudioSource = GetComponent<AudioSource>();
        musicAudioSource.enabled = (PrefrencesManager.Music == 1) ? true : false;
    }

    public void SwitchMusic(int value)
    {
        musicAudioSource.enabled = (value == 1) ? true : false;
    }
}
