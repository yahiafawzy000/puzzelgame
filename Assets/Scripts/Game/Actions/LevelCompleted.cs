﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace yf
{
    public class LevelCompleted : Action
    {
        public override void Execute()
        {
            Debug.Log("-------------------------->>>");
            //9/12 int currentLevel = SceneManager.GetActiveScene().buildIndex; // 1
            int currentLevel = LevelsManagers.instance.CurrentLevel;
            int openLevels   = PrefrencesManager.OpenLevels; //1
            int closedLevel  = PrefrencesManager.ClosedLevels; // 14
            if (currentLevel<closedLevel)
            {
                if (currentLevel == openLevels)
                {
                    OpenNewLevel(currentLevel+1);
                }
                InventoryMenu.instance.RestInentory();
                StartLevel(currentLevel+1);
                InventoryMenu.instance.SetLevelName(currentLevel+2);
            }
            else {
                Debug.Log("buy new levels");
            }
        }

        int id;

        private void StartLevel(int id)
        {
            this.id = id;
            ShowAdd();
        }


        void OpenNewLevel(int levelToOpen)
        {
            PrefrencesManager.OpenLevels = levelToOpen;
        }

        AsyncOperation asyncOperation;

        void ShowAdd()
        {
            AdManager.instance.ShowRewardedAd(AddDoneListener);
            //9/12 asyncOperation = SceneManager.LoadSceneAsync(id);
            //9/12 asyncOperation.allowSceneActivation = false;

        }

        IEnumerator LoadScene(int scene)
        {
            asyncOperation =
                 SceneManager.LoadSceneAsync(scene);
            asyncOperation.allowSceneActivation = false;
            while (!asyncOperation.isDone)
            {
                yield return new WaitForSecondsRealtime(0.1f);
            }
        }

        void AddDoneListener() {
            Debug.Log("level ------ "+id);
            //9/12 asyncOperation.allowSceneActivation = true;
            LevelsManagers.instance.OpenLevel(id);
        }
    }

}
