﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace yf
{
    public class PickupActableItem : PickupItem
    {
        public Texture _bigIcon;
        public AudioClip _audioClip;
        
        [Header("target item")]
        public Texture _targrtIcon;
        public Texture _targrtBigIcon;
        public Conditions _resultItemId;
        public ResultItemType _resultItemType;
        public AudioClip audioClip; 


        public override void Execute()
        {
            InventoryMenu.instance.AddItemToInventorySlot(this);
        }
    }
}
