﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace yf
{
    public class SwapRemoteImageAction : Action
    {
        [SerializeField]
        private RawImage target = null;
        [SerializeField]
        private Texture targetText;
        public override void Execute()
        {
            if (target&&targetText)
            {
                target.texture = targetText;    
            }
        }
    }
}