﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace yf
{
    public class ShowMaltiMask : Action
    {
        [SerializeField]
        GameObject []mask;
        public override void Execute()
        {
            for (int i = 0; i < mask.Length; i++)
            {
                if(mask[i])
                mask[i].SetActive(true);
            }
           
        }
    }
}
