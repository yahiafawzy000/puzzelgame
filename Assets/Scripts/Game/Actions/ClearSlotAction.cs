﻿using System.Collections;
using UnityEngine;

namespace yf
{
    public class ClearSlotAction : Action
    {
        [SerializeField]
        int id;

        public override void Execute()
        {
            InventoryMenu.instance.clearSlot(id);
        }
       
    }
}