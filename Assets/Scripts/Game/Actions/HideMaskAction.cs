﻿using UnityEngine;
using System.Collections;

namespace yf
{ 
    public class HideMaskAction : Action
    {
      
        [SerializeField]
        GameObject mask;
        public override void Execute()
        {
            if (mask != null)
            {
                Destroy(mask);
            }
        }
    }
}