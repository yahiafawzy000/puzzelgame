﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace yf
{
    public class ChangTargetAction : Action
    {
        [SerializeField]
        Scene swapImageAction;
        [SerializeField]
        Texture texture;
        public override void Execute()
        {
            swapImageAction.target = texture;
        }
    }
}