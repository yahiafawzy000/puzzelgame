﻿using UnityEngine;
using UnityEngine.UI;

namespace yf
{
    public class PickupItem : Action
    {
        [Header("My Data")]
        public int itemId;
        public Texture itemIcon;
        public bool KeepForEver, keepObjectPreview;
        
        public override void Execute()
        {
            InventoryMenu.instance.AddItemToInventorySlot(this);
        }
    }
    
}
