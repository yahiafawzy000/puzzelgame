﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace yf
{
    public class HideMultiMask : Action
    {
        [SerializeField]
        GameObject []mask;
        public override void Execute()
        {
            for(int i=0;i<mask.Length;i++) 
            {
                if (mask[i])
                    Destroy(mask[i]);
            }
        }
    }
}
