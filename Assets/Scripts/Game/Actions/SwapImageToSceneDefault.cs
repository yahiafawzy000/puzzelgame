﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace yf
{
    public class SwapImageToSceneDefault : Action
    {

        [SerializeField]
        public Scene scene = null;
        [SerializeField] Texture texture;


        public override void Execute()
        {
            if (scene)
            {
                scene.CheckImageToSetDefault(texture);
            }
        }
    }
}
