﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace yf
{
    public class ShowMaskAction : Action
    {
        [SerializeField]
        GameObject mask;
        public override void Execute()
        {
            if (mask != null)
            {
                mask.SetActive(true);
            }
        }
    }
}
