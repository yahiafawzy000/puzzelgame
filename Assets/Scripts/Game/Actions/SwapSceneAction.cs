﻿using UnityEngine;
namespace yf
{
    public class SwapSceneAction : Action
    {
        [SerializeField]
        private GameObject target = null;
        public override void Execute()
        {
            if (target)
            {
                target.SetActive(true);
                this.transform.parent.gameObject.SetActive(false);
            }
        }
    }
}


