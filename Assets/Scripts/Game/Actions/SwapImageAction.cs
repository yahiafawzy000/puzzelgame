﻿using UnityEngine;
using UnityEngine.UI;

namespace yf
{
    public class SwapImageAction : Action
    {
        [SerializeField]
        public Texture target=null;

        
        public override void Execute()
        {           
            if (target)
            {               
                this.transform.parent.gameObject.GetComponent<RawImage>().texture = target;              
            }
        }
    }
}
