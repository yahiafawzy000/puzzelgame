﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
namespace yf
{
    public class PickupMultiActableItem : PickupItem
    {
        public Texture _bigIcon;
        public AudioClip _audioClip;
        public bool _actableItself = false;

        [Header("target item")]
        public List<ActableData> actableData;



        public override void Execute() //1
        {
            InventoryMenu.instance.AddItemToInventorySlot(this);
        }
    }
}
