﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace yf
{
    public class ToggleObjectAction : Action
    {
        [SerializeField]
        private GameObject target = null;
        [SerializeField] SwapType swapType=SwapType.toggle;
        public override void Execute()
        {
            if (target)
            {
                if (swapType == SwapType.on)
                {
                    target.SetActive(true);
                }
                else if (swapType == SwapType.off)
                {
                    target.SetActive(false);
                }
                else {
                    target.SetActive(!target.activeSelf);
                }
            }
        }
    }


    public enum SwapType {on,off,toggle }
}
