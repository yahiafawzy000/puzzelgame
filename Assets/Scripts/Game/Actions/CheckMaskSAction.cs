﻿using UnityEngine;

namespace yf
{
    public class CheckMaskSAction : MonoBehaviour
    {
        [SerializeField]
        GameObject[] masks;

        public bool HaveAllNeededMasks()
        {
            for (int i = 0; i < masks.Length; i++) {
                if (!masks[i].activeSelf) return false;
            }
            return true;
        }       

    }
}