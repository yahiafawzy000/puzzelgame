﻿using System;
using UnityEngine;

namespace yf
{
    public abstract class Action : MonoBehaviour
    {
        public void OnEnable()
        {
            
        }
        public abstract void Execute();
    }
}