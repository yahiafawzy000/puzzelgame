﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

  namespace yf
{
    public class DestroyAction : Action
    {
        [SerializeField]
        private GameObject target = null;
        public override void Execute()
        {
            if (target)
            {
                Destroy(target);
            }
        }
    }
}
