﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace yf
{
    public class OpenSubScene : Action
    {
        [SerializeField]
        private GameObject target = null;
        public override void Execute()
        {
            if (target)
            {
                target.SetActive(true);
            }
        }
    }
}