﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace yf
{
    public class ClickacbleUI : MonoBehaviour, IPointerDownHandler
    {
       private Action[]actions;

        [SerializeField]
        bool HideImage;

        [SerializeField]
        protected bool DesOnFinish = false;

        private void Start() {
            //#if !UNITY_EDITOR
                       //  GetComponent<RawImage>().color = new Color(0, 0, 0,0);
            //#endif
            if (HideImage)
            {
                Color color = GetComponent<RawImage>().color;
                GetComponent<RawImage>().color = new Color(color.r, color.g, color.b, 0);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            //
            CheckMaskSAction masks = GetComponent<CheckMaskSAction>();
            if (masks)
            {
                if (!masks.HaveAllNeededMasks()) {
                    return;
                }
            }

            PreCondtion condition = GetComponent<PreCondtion>();
            if (condition) {
                condition.IsDone = condition.CheckNeededCondition();
            }
            if (!condition || condition.IsDone)
            {
                PlaySound(true);
                ExecuteActions();
                if (DesOnFinish)
                    Destroy(gameObject);
            }
            else {
                PlaySound(false);
            }
       }

        private void PlaySound(bool state)
        {
            SoundTrack soundTrack = GetComponent<SoundTrack>();
            if (soundTrack) {
                soundTrack.PlaySound(state);
            }
        }

        private void ExecuteActions() {
            actions = GetComponents<Action>();
            foreach (Action action in actions)
            {
                action.Execute();
            }
        }
      
    }
}