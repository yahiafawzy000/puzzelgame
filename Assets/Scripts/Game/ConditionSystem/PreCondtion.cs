﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace yf
{
    /// <summary>
    /// check if this condition is done if the id  of selected item in iventory == condition id
    /// </summary>
    public class PreCondtion : Condition
    {
        public bool CheckNeededCondition()
        {
            int num = 0;
            if (InventoryMenu.instance.selectedSlot) //inventorty have selected item
            {
                num = InventoryMenu.instance.selectedSlot._itemId;
            }
           
            //if (num == 0) return IsDone; //its done or no one selected
            if (num == (int)conditionToStart)
            {
                IsDone = true;

                if (!InventoryMenu.instance.selectedSlot.keepObjectForever)
                {
                    //InventoryMenu.instance.selectedSlot.SwitchSlot();
                    InventoryMenu.instance.clearSlot(InventoryMenu.instance.selectedSlot,true);
                }

                return true;
            }
            else
            {
                return false;
            }

        }
    }
}