﻿using UnityEngine;
namespace yf
{
    public abstract class Condition : MonoBehaviour
    {
        [SerializeField]
        protected Conditions conditionToStart;
        [SerializeField]
        public bool IsDone;
    }
}