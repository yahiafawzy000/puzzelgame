﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace yf
{
    public class ResultCondition : Condition
    {
        public void SetConditionToDone()
        {
            int num = (int)conditionToStart;
            ConditionManager.instance.ConditionsStatus[num - 1] = true;
        }
    }
}
