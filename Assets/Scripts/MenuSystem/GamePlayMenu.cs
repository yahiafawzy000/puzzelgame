﻿using UnityEngine;

public class GamePlayMenu : AMenu
{
   // [SerializeField]
   // GameObject gamePanel;

    private void Start()
    {
       // MenuManager.instance.GamePlayPanel = gamePanel;
    }

    public void SettingClicked() {
        ButtonCliked(); 
        //gamePanel.SetActive(false);
        MenuManager.instance.HideShowMenus(true);
        MenuManager.instance.ActiveResumeButton();
    }

    public void HelpCliked() {
        ButtonCliked();        
        MenuManager.instance.OpenMenuInGame(MenuName.QuestionMarkMenu);
    }

    protected override void ExitMenu()
    {
       
    }

    protected override void StartMenu()
    {
       
    }
}
