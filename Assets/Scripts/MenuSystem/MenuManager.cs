﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public static MenuManager instance;
    [Tooltip("main menu = 1 , level selction = 2 ,optionMenu = 3")]
    [SerializeField]
    private GameObject[] MenusPrefabs;
    private GameObject[] Menus = new GameObject[8];

    //public GameObject menus;
   

    internal void ActiveResumeButton()
    {
        //GamePlayPanel = gamePanel;
        Menus[0].GetComponent<MainMenu>().ActiveResumeButton(true);   
    }

    private void Awake()
    {
        if (!instance)
        {
            instance = this;           
        }
        else {
            Destroy(gameObject);        
        }
    }

    private void Start()
    {
        //if(SceneManager.GetActiveScene().buildIndex==0)
        OpenMenu(0);        
    }

    public void HideShowMenus(bool state=false) {        
        gameObject.SetActive(state);
    }

    public void OpenMenu(MenuName menuName) {
        int index = (int)menuName;      
        if (Menus[index] == null)
        {
            Menus[index] = Instantiate(MenusPrefabs[index], transform);
        }
        else {
            Menus[index].SetActive(true);
            Menus[index].transform.parent = transform; 
        }
    }

    public void OpenMenuInGame(MenuName menuName) {
        int index = (int)menuName;
        Instantiate(MenusPrefabs[index], LevelsManagers.instance. Level.transform);          
    }

    public void DestroyMenu(MenuName menuName) {
        int index = (int)menuName;
        if (Menus[index] == null)
        {
            Destroy(Menus[index]);
        }       
    }
    
    public void DisableMenu(MenuName menuName)
    {
        int index = (int)menuName;
        if (Menus[index] != null)
        {
            Debug.Log(index);
            Menus[index].SetActive(false);
        }       
    }

    public void LoadSceneAs(int index) {
        StartCoroutine(LoadScene(index));
    }

    IEnumerator LoadScene(int scene)
    {
        AsyncOperation asyncOperation = 
             SceneManager.LoadSceneAsync(scene);       
        while (!asyncOperation.isDone)
        {           
            yield return new WaitForSecondsRealtime(0.1f);
        }
    }
}

public enum MenuName { 
 MainMenu  = 0 , LevelSelctionMenu = 1 , OptionMenu = 2, HowToPlayMenu=3,WalkThrowMenu=4,BounsMenu=5,QuestionMarkMenu=6, GamePlayBounsBounsMenu
}
