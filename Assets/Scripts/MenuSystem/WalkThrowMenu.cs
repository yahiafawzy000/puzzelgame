﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace yf
{
    public class WalkThrowMenu : AMenu
    {
        [Header("level button")]
        [SerializeField]
        Texture openTexture, closedTexture, unavalableTexture;
        [Header("level Progress")]
        int open, closed, unavalable;
       
        private RawImage[] levels;

        public static string[] youTubeLinks = { 
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s",
        "https://www.youtube.com/watch?v=4qLJlMpPdK0&t=168s"};

      

        protected override void ExitMenu()
        {
        }
        private void addIsDone()
        {
            Application.OpenURL(youTubeLinks[clikedButton]);
        }

        protected override void StartMenu()
        {
            //PrefrencesManager.ClosedLevels = 14;
            open = PrefrencesManager.OpenLevels;
            closed = PrefrencesManager.ClosedLevels;
            unavalable = PrefrencesManager.UnAvalableLevels;
            Debug.Log("startMenu  " + open + "  " + closed + "  " + unavalable);
            GetLevelsRawImages();
            SetupLevels();
        }

        public void RemoveAds()
        {
            ButtonCliked();
            MenuManager.instance.OpenMenu(MenuName.BounsMenu);
        }

        private void GetLevelsRawImages()
        {
            levels = transform.GetChild(1).GetComponentsInChildren<RawImage>();
        }

        private void SetupLevels()
        {
            Debug.Log("error");
            TMPro.FontStyles fontStyle = levels[0].transform.GetChild(0).GetComponent<TMP_Text>().fontStyle;
            TMP_Text tMP_Text;
            for (int i = 0; i < open; i++)
            {
                levels[i].texture = openTexture;
                tMP_Text = levels[i].transform.GetChild(0).GetComponent<TMP_Text>();
                tMP_Text.text = (i + 1) + "";
                tMP_Text.fontStyle = fontStyle;
                tMP_Text.color = Color.black;
                int iCopy = i;
                levels[i].gameObject.AddComponent<Button>().onClick.AddListener(delegate { _addLinkToButton(iCopy); });
            }
            for (int i = open; i < closed; i++)
            {
                levels[i].texture = closedTexture;
                tMP_Text = levels[i].transform.GetChild(0).GetComponent<TMP_Text>();
                tMP_Text.text = "";
            }
            for (int i = closed; i < unavalable; i++)
            {
                levels[i].texture = unavalableTexture;
                tMP_Text = levels[i].transform.GetChild(0).GetComponent<TMP_Text>();
                tMP_Text.text = "";
                levels[i].gameObject.AddComponent<Button>().onClick.AddListener(delegate
                {
                    MenuManager.instance.OpenMenu(MenuName.BounsMenu);
                });
            }
        }

        private int clikedButton=-1;

        public void _addLinkToButton(int i)
        {
            ButtonCliked();
            clikedButton = i;
            AdManager.instance.ShowRewardedAd(addIsDone);  
        }

        public void Back()
        {
            ButtonCliked();
            MenuManager.instance.DisableMenu(MenuName.WalkThrowMenu);
        }
    }
}