﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounsMenu : AMenu
{
    public virtual void Back()
    {
        ButtonCliked();
        MenuManager.instance.DisableMenu(MenuName.BounsMenu);
    }

    public void AllBouns()
    {
        ButtonCliked();
        IAPManager.instance.BounsAll();
    }

    public void Bouns1()
    {
        ButtonCliked();
        IAPManager.instance.Bouns1();
    }
    public void Bouns2()
    {
        ButtonCliked();
        IAPManager.instance.Bouns2();
    }
    public void Bouns3()
    {
        ButtonCliked();
        IAPManager.instance.Bouns3();
    }
    public void RemoveAds()
    {
        ButtonCliked();
        IAPManager.instance.ReMoveAdds();
    }
    public void Rate ()
    {
        ButtonCliked();       
#if UNITY_ANDROID
            Application.OpenURL("market://details?id=com.ticktick.task");
#elif UNITY_IPHONE
		Application.OpenURL("itms-apps://itunes.apple.com/app/idYOUR_ID");
#endif        
    }
    public void Restore()
    {
        ButtonCliked();
        //MenuManager.instance.DisableMenu(MenuName.BounsMenu);
    }

    protected override void ExitMenu()
    {

    }

    protected override void StartMenu()
    {
    }
}
