﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using yf;
namespace yf
{
    public class LevelSelectionMenu : AMenu
    {
        [Header("level button")]
        [SerializeField]
        Texture openTexture, closedTexture, unavalableTexture;
        [Header("level Progress")]
        int open, closed, unavalable;
        [Header("Load Screen")]
        [SerializeField]
        GameObject loadSCreen;

        private RawImage[] levels;

        protected override void ExitMenu()
        {

        }

        protected override void StartMenu()
        {

            Slider slider = loadSCreen.GetComponentInChildren<Slider>();
            slider.value = 0;
            //PrefrencesManager.ClosedLevels = 14;

            open = PrefrencesManager.OpenLevels;
            closed = PrefrencesManager.ClosedLevels;
            unavalable = PrefrencesManager.UnAvalableLevels;
            Debug.Log("startMenu  " + open + "  " + closed + "  " + unavalable);
            GetLevelsRawImages();
            SetupLevels();
        }

        private void GetLevelsRawImages()
        {
            levels = transform.GetChild(1).GetComponentsInChildren<RawImage>();
        }

        private void SetupLevels()
        {
            Debug.Log("error");
            TMPro.FontStyles fontStyle = levels[0].transform.GetChild(0).GetComponent<TMP_Text>().fontStyle;
            TMP_Text tMP_Text;
            for (int i = 0; i < open; i++)
            {
                levels[i].texture = openTexture;
                tMP_Text = levels[i].transform.GetChild(0).GetComponent<TMP_Text>();
                tMP_Text.text = (i + 1) + "";
                tMP_Text.fontStyle = fontStyle;
                tMP_Text.color = Color.black;
                int iCopy = i;
                levels[i].gameObject.AddComponent<Button>().onClick.AddListener(delegate { _OpenMenu_OpenMenu(iCopy); });
            }
            for (int i = open; i < closed; i++)
            {
                levels[i].texture = closedTexture;
                tMP_Text = levels[i].transform.GetChild(0).GetComponent<TMP_Text>();
                tMP_Text.text = "";
            }
            for (int i = closed; i < unavalable; i++)
            {
                levels[i].texture = unavalableTexture;
                tMP_Text = levels[i].transform.GetChild(0).GetComponent<TMP_Text>();
                tMP_Text.text = "";
                levels[i].gameObject.AddComponent<Button>().onClick.AddListener(delegate 
                {
                    MenuManager.instance.OpenMenu(MenuName.BounsMenu); 
                });
            }
        }

        public void _OpenMenu_OpenMenu(int i)
        {
            ButtonCliked();
            loadSCreen.SetActive(true);
            if(InventoryMenu.instance)
            InventoryMenu.instance.RestInentory();

            // 12/9 StartCoroutine(LoadScene(i + 1));
            //PrefrencesManager.LevelToOpen = i;
            StartCoroutine(LoadScene(i));
            //LoadSceneMethod(i + 1);
        }


        IEnumerator LoadScene(int level)
        {
            LevelsManagers.instance.LoadLevel(level); 

            Slider slider = loadSCreen.GetComponentInChildren<Slider>();
            slider.value = 0;
            //AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(scene);
            //asyncOperation.allowSceneActivation = false;
            while (slider.value < 0.9f)
            {
                slider.value += 0.1f;
                yield return new WaitForSeconds(0.05f);
            }
            //asyncOperation.allowSceneActivation = true;
            // if (!asyncOperation.isDone)
            // {
            //  yield return asyncOperation.isDone;
            //}
            LevelsManagers.instance.ActiveLevel();
            MenuManager.instance.HideShowMenus();
            Destroy(this.gameObject);
            StopAllCoroutines();
        }
        public void Back()
        {
            ButtonCliked();
            MenuManager.instance.DisableMenu(MenuName.LevelSelctionMenu);
        }

    }
}