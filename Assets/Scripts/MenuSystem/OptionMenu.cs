﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class OptionMenu : AMenu
{

    [SerializeField]
    TMP_Text music, sound;
  
    public void Back() {
        ButtonCliked(); 
        MenuManager.instance.DisableMenu(MenuName.OptionMenu);
    }

    public void HowToPlay()
    {
        ButtonCliked();
        MenuManager.instance.OpenMenu(MenuName.HowToPlayMenu);
    }

    public void Music()
    {
        if (PrefrencesManager.Music == 1)
        {
            music.color = Color.red;
            PrefrencesManager.Music = 0;
            music.text = "off";
            MusicManager.instance.SwitchMusic(0);
        }
        else
        {
            music.color = Color.green;
            music.text = "on";
            PrefrencesManager.Music = 1;
            MusicManager.instance.SwitchMusic(1);
        }
        ButtonCliked();
    }

    public void Sound()
    {
        if (PrefrencesManager.Sound == 1)
        {
            sound.color = Color.red;
            sound.text = "off";
            PrefrencesManager.Sound = 0;

            music.color = Color.red;
            music.text = "off";
            PrefrencesManager.Music = 0;
            MusicManager.instance.SwitchMusic(0);
        }
        else {
            sound.color = Color.green;
            sound.text = "on";
            PrefrencesManager.Sound = 1;
        }

        ButtonCliked();
    }

    public void MoreGames()
    {
        ButtonCliked();
#if UNITY_ANDROID
        Application.OpenURL("https://play.google.com/store/apps/dev?id=8638120915920097780");
#elif UNITY_IPHONE
		   Application.OpenURL("itms-apps://itunes.apple.com/app/idYOUR_ID");
#endif    
    }

    public void Rate()
    {
        ButtonCliked();
#if UNITY_ANDROID
        Application.OpenURL("market://details?id=com.ticktick.task");
#elif UNITY_IPHONE
		Application.OpenURL("itms-apps://itunes.apple.com/app/idYOUR_ID");
#endif        
    }

    protected override void StartMenu()
    {
        if (PrefrencesManager.Music == 1)
        {
            music.text = "On";
            music.color = Color.green;
            MusicManager.instance.SwitchMusic(1);
        }
        else
        {
            music.text = "off";
            music.color = Color.red;
            MusicManager.instance.SwitchMusic(0);
        }

        if (PrefrencesManager.Sound == 1)
        {
            sound.color = Color.green;
            sound.text = "on";
        }
        else
        {
            sound.color = Color.red;
            sound.text = "off";
        }
    }

    public void WalkThrow() {
        ButtonCliked();
        MenuManager.instance.OpenMenu(MenuName.WalkThrowMenu);
    }

    protected override void ExitMenu()
    {
        //throw new System.NotImplementedException();
    }
}
