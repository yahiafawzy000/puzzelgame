﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class HowToPlayMenu : AMenu
{
    public void Back() {
        ButtonCliked();
        MenuManager.instance.DisableMenu(MenuName.HowToPlayMenu);
    }

    protected override void ExitMenu()
    {
       // throw new System.NotImplementedException();
    }

    protected override void StartMenu()
    {
       // throw new System.NotImplementedException();
    }
}
