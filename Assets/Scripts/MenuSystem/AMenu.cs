﻿using UnityEngine;

public abstract class AMenu : MonoBehaviour
{
    [HideInInspector]
    public AMenu instance;
    protected virtual void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        StartMenu();
    }

    private void OnDestroy()
    {
        ExitMenu();
    }


    protected void ButtonCliked() {
        SoundManager.instance.PlayButtonPressedSound(); 
    }

    protected abstract void StartMenu();

    protected abstract void ExitMenu();

}
