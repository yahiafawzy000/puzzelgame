﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : AMenu
{
    [SerializeField]
    Button ResumeButton;

    private void Start()
    {
        ResumeButton.interactable = false;    
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Application.Quit();
        }
    }
    public void Play() {
        ButtonCliked();
        MenuManager.instance.OpenMenu(MenuName.LevelSelctionMenu);
    }

    public void Resume() {
        ButtonCliked();
        MenuManager.instance.HideShowMenus(false);
    }

    public void Option() {
        ButtonCliked();
        MenuManager.instance.OpenMenu(MenuName.OptionMenu);
    }

    internal void ActiveResumeButton(bool state)
    {
        ResumeButton.interactable = state;
    }

    public void Bonus() {
        ButtonCliked();
        MenuManager.instance.OpenMenu(MenuName.BounsMenu);
    }

    protected override void StartMenu()
    {

    }

    protected override void ExitMenu()
    {
        //throw new System.NotImplementedException();
    }
}
