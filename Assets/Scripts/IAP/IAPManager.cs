﻿using System;
using UnityEngine;
using UnityEngine.Purchasing;


public class IAPManager : MonoBehaviour, IStoreListener
{
    public static IAPManager instance;

    private static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;

    //Step 1 create your products
    private string reMoveAds = "remove_ads";
    private string bouns1 = "bouns1";
    private string bouns2 = "bouns2";
    private string bouns3 = "bouns3";
    private string bounsAll = "bouns_all";

    //{remove_ads,bouns1,bouns2,bouns3,bouns_all }



    //************************** Adjust these methods **************************************
    public void InitializePurchasing()
    {
        if (IsInitialized()) { return; }
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        //Step 2 choose if your product is a consumable or non consumable
        builder.AddProduct(reMoveAds, ProductType.NonConsumable);
        builder.AddProduct(bouns1,    ProductType.Consumable);
        builder.AddProduct(bouns2,    ProductType.Consumable);
        builder.AddProduct(bouns3,    ProductType.Consumable);
        builder.AddProduct(bounsAll,  ProductType.Consumable);
        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }


    //Step 3 Create methods
    public void ReMoveAdds() { 
        BuyProductID(reMoveAds);
    }

    public void Bouns1() {
        BuyProductID(bouns1);
    }

    public void Bouns2()
    {
        BuyProductID(bouns2);
    }
    public void Bouns3()
    {
        BuyProductID(bouns3);
    }
    public void BounsAll()
    {
        BuyProductID(bounsAll);
    }


    //Step 4 modify purchasing
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        if (String.Equals(args.purchasedProduct.definition.id, reMoveAds, StringComparison.Ordinal))
        {
            Debug.Log(reMoveAds);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, bouns1, StringComparison.Ordinal))
        {
            Debug.Log(bouns1);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, bouns2, StringComparison.Ordinal))
        {
            Debug.Log(bouns2);
        }
       else if (String.Equals(args.purchasedProduct.definition.id, bouns3, StringComparison.Ordinal))
        {
            Debug.Log(bouns3);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, bounsAll, StringComparison.Ordinal))
        {
            Debug.Log(bounsAll);
        }
        else
        {
            Debug.Log("Purchase Failed");
        }
        return PurchaseProcessingResult.Complete;
    }










    //**************************** Dont worry about these methods ***********************************
    private void Awake()
    {
        TestSingleton();
    }

    void Start()
    {
        if (m_StoreController == null) { InitializePurchasing(); }
    }

    private void TestSingleton()
    {
        if (instance != null) { Destroy(gameObject); return; }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            Debug.Log("RestorePurchases started ...");

            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) => {
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else
        {
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}