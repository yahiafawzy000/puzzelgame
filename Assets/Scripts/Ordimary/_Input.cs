﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _Input : MonoBehaviour
{
    public Vector2 velocity;
    // Update is called once per frame
    void Update()
    {
        velocity = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
    }
}
