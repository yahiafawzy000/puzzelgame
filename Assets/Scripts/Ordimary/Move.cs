﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    [SerializeField]
    float speed = 10;
    _Input input;
    // Start is called before the first frame update
    void Start()
    {
       input = GetComponent<_Input>();   
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += (Vector3)input.velocity * Time.deltaTime * speed;
    }
}
