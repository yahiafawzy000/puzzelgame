﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundTrack : MonoBehaviour
{
    public AudioClip positive,negtive;
    // Start is called before the first frame update

    public void PlaySound(bool state) {
        Debug.Log(state);
        if (state)
        {
            PlayPositiveSound(); 
        }
        else {
            PlayNegtiveSound();
        }
    }

    private void PlayNegtiveSound()
    {
        if (negtive) {
            SoundManager.instance.playSound(negtive);
        }
    }

    private void PlayPositiveSound()
    {
        if (positive)
        {
            SoundManager.instance.playSound(positive);
        }
    }
}
