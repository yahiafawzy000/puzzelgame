﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRocket : MonoBehaviour
{
    Rigidbody2D rigidbody2;
    BoxCollider2D collider2D;
    Vector2 velocitey;
    [SerializeField]
    float MaxSpeed = 10;

    [SerializeField]
    float accelartion = 10,up=30;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody2 = GetComponent<Rigidbody2D>();
    }


    private void Update()
    {
        velocitey.y = Input.GetAxisRaw("Vertical");
        velocitey.x = Input.GetAxisRaw("Horizontal");        
    }

    Vector2 cashedVelocitey;
    Vector2 speedVector;
    float GetVelociteyX() {
        if (velocitey.x == 0)
        {
           /// speedVector.x = Mathf.Lerp(speedVector.x, 0, 0.01f * accelartion * Time.deltaTime);

           return Mathf.Lerp(cashedVelocitey.x,
               0, 0.01f*accelartion*Time.deltaTime);
        }
        else
        {
            // speedVector.x = Mathf.Lerp(speedVector.x, MaxSpeed , 0.01f * accelartion * Time.deltaTime);
            return Mathf.Lerp(cashedVelocitey.x,
                           velocitey.x, 0.01f * up * Time.deltaTime);
        }
    }

    float cSpeed=0,speed;
    float Speed()
    {
        if (velocitey.y==0)
        {
            /// speedVector.x = Mathf.Lerp(speedVector.x, 0, 0.01f * accelartion * Time.deltaTime);

            return Mathf.Lerp(cSpeed,
                0, 0.01f * accelartion * Time.deltaTime);
        }
        else
        {
            // speedVector.x = Mathf.Lerp(speedVector.x, MaxSpeed , 0.01f * accelartion * Time.deltaTime);
            return Mathf.Lerp(cSpeed,
                           MaxSpeed, 0.01f * up * Time.deltaTime);
        }
    }

    float GetVelociteyY()
    {
        if (velocitey.y == 0)
        {
           // speedVector.y = Mathf.Lerp(speedVector.y, 0, 0.01f * accelartion * Time.deltaTime);
            return Mathf.Lerp(cashedVelocitey.y,
                0, 0.01f * accelartion * Time.deltaTime);
        }
        else
        {
            //  speedVector.y = Mathf.Lerp(speedVector.y, MaxSpeed, 0.01f * accelartion * Time.deltaTime);
          return Mathf.Lerp(cashedVelocitey.y,
                velocitey.y, 0.01f * up * Time.deltaTime);
        }
    }

    void FixedUpdate()
    {
       
        //cashedVelocitey.x = GetVelociteyX();
        cashedVelocitey.y = GetVelociteyY();
        cSpeed = Speed();
        transform.rotation = Quaternion.Euler(0,0, transform.rotation.eulerAngles.z+velocitey.x*-10);
        if (cashedVelocitey.magnitude != 0)
         rigidbody2.velocity = transform.up * cSpeed * Time.deltaTime;

                //rigidbody2.velocity = cashedVelocitey * MaxSpeed * Time.deltaTime;
                else rigidbody2.velocity = Vector2.zero;
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 0.75f);
        Gizmos.DrawRay(transform.localPosition, transform.up * 2);
        if(rigidbody2)
        Gizmos.DrawRay(transform.localPosition, rigidbody2.velocity*1);
    }
}
